import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import cross_val_score, train_test_split

# Downloaded from https://archive.ics.uci.edu/ml/machine-learning-databases/housing/
# Needs a little manual pre-processing to become a proper CSV file (5 minutes)
boston_houses_csv_file = r'E:\Workspace\learn-by-example\datasets\boston_housing\housing.csv'

# We shall predict the value of the home (MEDV)

'''
Attribute Information:

    1. CRIM      per capita crime rate by town
    2. ZN        proportion of residential land zoned for lots over 
                 25,000 sq.ft.
    3. INDUS     proportion of non-retail business acres per town
    4. CHAS      Charles River dummy variable (= 1 if tract bounds 
                 river; 0 otherwise)
    5. NOX       nitric oxides concentration (parts per 10 million)
    6. RM        average number of rooms per dwelling
    7. AGE       proportion of owner-occupied units built prior to 1940
    8. DIS       weighted distances to five Boston employment centres
    9. RAD       index of accessibility to radial highways
    10. TAX      full-value property-tax rate per $10,000
    11. PTRATIO  pupil-teacher ratio by town
    12. B        1000(Bk - 0.63)^2 where Bk is the proportion of blacks 
                 by town
    13. LSTAT    % lower status of the population
    14. MEDV     Median value of owner-occupied homes in $1000's
'''

if __name__ == '__main__':
    df = pd.read_csv(boston_houses_csv_file)
    target_column = 'MEDV'
    test_size = 0.3
    cross_validation_folds = 5
    acceptable_error = 4

    y = df[target_column]  # Series
    X = df.drop(target_column, axis=1)  # DataFrame

    # Train/Test split
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=42)

    # Lets try fitting a linear regression
    lin_reg = LinearRegression()
    lin_reg.fit(X=X_train, y=y_train)
    score_on_test_set = lin_reg.score(X_test, y_test)

    samples_trained_on = int((1 - test_size) * len(y))
    print(
            '\n\nGoodness of fit after training over {:d} samples is {:.3f}'.format(samples_trained_on,
                                                                                    score_on_test_set))
    cross_scores = cross_val_score(lin_reg, X=X_train, y=y_train, scoring='r2', cv=cross_validation_folds, n_jobs=-1)
    cross_score = np.mean(cross_scores)
    print('Cross validated goodness of fit with {:d} folds is {:.3f}'.format(cross_validation_folds,
                                                                             cross_score))

    # We shall compare this vector with the real vector y_test
    # Note it was already done by calling the score function above.

    y_predicted = lin_reg.predict(X_test)

    quite_correct_predictions = 0
    print('\nLets see how well did the prediction go:')
    for sample, prediction in enumerate(y_predicted):
        actual_value = y_test.iloc[sample]
        error = np.abs(prediction - actual_value)
        if error < acceptable_error:
            quite_correct_predictions += 1
        print('predicted = {:.1f}'.format(prediction),
              'actual value = {:.1f}'.format(actual_value), '\t\t--> quite-close' if error < acceptable_error else '')

    print('\nOut of {:d} predictions, got {:d} ({:.1f}%) quite correctly.'.format(
            len(y_predicted),
            quite_correct_predictions,
            100 * quite_correct_predictions / len(y_predicted)
    ))
