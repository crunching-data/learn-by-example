import datetime
import operator
import os

from sklearn.externals import joblib  # Serialization (of the trained model)
from sklearn.neighbors import KNeighborsClassifier

from knn_classification.digit_recognition import DigitRecognition


def time_it(is_stop: bool, message=None) -> None:
    global timer
    global working_on

    if not is_stop:
        working_on = message
        timer = datetime.datetime.now()
        print(working_on)
        return

    delta = datetime.timedelta(seconds=(datetime.datetime.now() - timer).total_seconds())
    print('{message} took {time}'.format(message=working_on, time=str(delta).split('.')[0]))


# Downloaded from http://yann.lecun.com/exdb/mnist/
handwritten_digits_folder = r'e:\Workspace\learn-by-example\datasets\handwritten_digits'
user_draws_digits_folder = r'e:\Workspace\learn-by-example\resources\user_drawn_digits'
trained_knn_model_file = r'e:\Workspace\learn-by-example\resources\objects_dump\trained_knn.joblib'

if __name__ == '__main__':
    working_on = ''
    timer = None

    time_it(False, 'Reading hand-written-digits dataset')
    train_images, train_labels, test_images, test_labels = DigitRecognition.get_all_data(handwritten_digits_folder)
    time_it(True)

    # If an already trained model does not exist
    # Train one and serialize it
    if not os.path.isfile(trained_knn_model_file):
        # Best one seems to be 3, next one is 7
        possible_k_values = [1, 3, 5, 7, 9, 11, 13, 15]
        scores_for_k_values = []

        for k in possible_k_values:
            print('\nTrying out k={}\n'.format(k))
            knn = KNeighborsClassifier(n_neighbors=k, n_jobs=-1)
            time_it(False, 'Training model')
            knn.fit(X=train_images, y=train_labels)
            time_it(True)

            time_it(False, 'Calculating score')
            score = knn.score(test_images, test_labels)
            print('Score for k={} is {}'.format(k, score))
            scores_for_k_values.append(score)
            time_it(True)

        '''
            # Perhaps a more accurate approach would be:
            
            time_it(True, 'Grid searching for the best K value')
            param_grid = {'n_neighbors': [1, 3, 5, 7, 9, 11, 13, 15]}
            knn = KNeighborsClassifier()
            knn_cv = GridSearchCV(estimator=knn, param_grid=param_grid, cv=5, n_jobs=-1)
            knn_cv.fit(X=train_images, y=train_labels)
            print('Best K value: {}'.format(knn_cv.best_params_))
            print('Best score: {}'.format(knn_cv.best_score_))
            time_it(False)
            
            # Note that due to the cross-validation of the score, this approach is
              Very expensive computationally.
        
        '''

        max_index, max_value = max(enumerate(scores_for_k_values), key=operator.itemgetter(1))
        best_k_value = possible_k_values[max_index]
        print('\nMaximal score of {} was achieved using k={}\n'.format(max_value, best_k_value))

        time_it(False, 'Learning again over all labeled data, using the best k value found')
        print('\nOK, let me re-study with the best value of {}\n'.format(best_k_value))
        knn = KNeighborsClassifier(n_neighbors=best_k_value, n_jobs=-1)
        print('Tests were run on {} samples.'.format(len(train_labels)))
        train_images.extend(test_images)
        train_labels.extend(test_labels)
        print('Final study is performed on {} samples.'.format(len(train_labels)))
        knn.fit(X=train_images, y=train_labels)
        time_it(True)

        time_it(False, 'Serializing')
        joblib.dump(knn, trained_knn_model_file, compress=9)
        time_it(True)

    else:
        print('Loading an existing pre-trained model...')
        knn = joblib.load(trained_knn_model_file)

    directory = os.fsencode(user_draws_digits_folder)

    print('\nOK, lets see...\n')
    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        file_full_path = os.path.join(user_draws_digits_folder, file.decode("utf-8"))
        if filename.endswith(".png") and os.path.getsize(file_full_path) < 1500:
            file_content = DigitRecognition.png_to_byte_array(file_full_path)
            # noinspection PyBroadException
            try:
                guess = knn.predict(file_content)[0]
                print('{} seems like {}'.format(filename, guess))
            except:
                print('Oops, something went wrong with this file.')
                continue
