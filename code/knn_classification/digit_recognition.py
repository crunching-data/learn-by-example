import os
import struct
import typing

import numpy as np


class DigitRecognition:
    train_file_names = dict(images='train-images.idx3-ubyte', labels='train-labels.idx1-ubyte')
    test_file_names = dict(images='t10k-images.idx3-ubyte', labels='t10k-labels.idx1-ubyte')

    label_values = []
    images = []

    @staticmethod
    def get_label_image_tuple(index: int) -> typing.Dict[int, list]:
        return dict(label=DigitRecognition.label_values[index], image=DigitRecognition.images[index])

    @staticmethod
    def iterator_on_samples(path: str, is_train: bool):
        files = DigitRecognition.train_file_names if is_train else DigitRecognition.test_file_names

        # Reading the labels.
        labels_file_path = os.path.join(path, files.get('labels'))
        with open(file=labels_file_path, mode='rb') as labels_file:
            _, number_of_samples = struct.unpack('>II', labels_file.read(8))
            # Each byte represents the actual value of the sample
            DigitRecognition.label_values = np.fromfile(labels_file, dtype=np.int8)

        # Reading the images.
        images_file_path = os.path.join(path, files.get('images'))
        with open(file=images_file_path, mode='rb') as images_file:
            _, number_of_samples, sample_height, sample_width = struct.unpack(">IIII", images_file.read(16))
            # Using ndarray.reshape()
            DigitRecognition.images = np.fromfile(images_file, dtype=np.uint8).reshape(
                    (len(DigitRecognition.label_values),
                     sample_height * sample_width))

        # Create an iterator which returns each image in turn
        for i in range(len(DigitRecognition.label_values)):
            # if i > 500:
            #     raise StopIteration
            yield DigitRecognition.get_label_image_tuple(i)

    @staticmethod
    def show(image):
        """
        Render a given numpy.uint8 2D array of pixel data.
        """
        from matplotlib import pyplot
        import matplotlib as mpl
        new_figure = pyplot.figure()
        ax = new_figure.add_subplot(1, 1, 1)
        imgplot = ax.imshow(image, cmap=mpl.cm.Greys)
        imgplot.set_interpolation('nearest')
        ax.xaxis.set_ticks_position('top')
        ax.yaxis.set_ticks_position('left')
        pyplot.show()

    @staticmethod
    def get_all_data(folder_path: str):
        training_samples = DigitRecognition.iterator_on_samples(folder_path, is_train=True)
        test_samples = DigitRecognition.iterator_on_samples(folder_path, is_train=False)

        train_images = []
        train_labels = []
        test_images = []
        test_labels = []
        for sample in training_samples:
            train_images.append(sample['image'])
            train_labels.append(sample['label'])
        for sample in test_samples:
            test_images.append(sample['image'])
            test_labels.append(sample['label'])

        return train_images, train_labels, test_images, test_labels

    @staticmethod
    def png_to_byte_array(png_path: str):
        from PIL import Image
        import numpy as np

        img = Image.open(png_path)
        arr = np.array(img)
        return [arr.flatten()]
